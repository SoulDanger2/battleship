/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

/**
 *
 * @author Victor
 */
public class Tablero {

    public static final byte JUGADOR = 1;
    public static final byte HORIZONTAL = 0;
    public static final byte TAMANO_BARCO = 0;
    byte contBarco1 = 4;
    byte contBarco2 = 3;
    byte contBarco3 = 2;
    byte contBarco4 = 1;

    //array para el tablero.
    public byte[][] arrayTablero = new byte[10][10];
    // Contadores para almacenar el número de fichas colocadas por
    //  cada jugador. Se deja la celda 0 sin uso.
    private byte[] numBarcosJugador = new byte[10];
    //array para el contador de los barcos.
    public byte[] arrayContBarcos = {0, 1, 2, 3, 4};

    //Bucle para inicializar y que empieze todo el tablero en 0
    public Tablero() {
        for (byte x = 0; x < arrayTablero.length; x++) {
            for (byte y = 0; y < arrayTablero[x].length; y++) {
                arrayTablero[x][y] = 0;
            }
        }
        /* for (byte x = 1; x < arrayContBarcos.length; x++) {
            arrayContBarcos[x] = 0;
        }*/

    }

    /*  private void mostrar() {
        // TODO: Hacer esto con bucles
        //Bucle para mostrar las posiciones del tablero.
        for (byte x = 0; x < arrayTablero.length; x++) {
            for (byte y = 0; y < arrayTablero[x].length; y++) {
                System.out.print(arrayTablero[x][y]);

            }
            System.out.println("");

        }

    }*/
    public boolean ponerBarco(int x, int y, int tamanoBarco, int orientacion) {
        try {
            // Si hay un barco en esa posición, retornar false
            if (arrayTablero[x][y] > 0) {
                return false;
            } else {
                // Poner el barco
                arrayTablero[x][y] = (byte) tamanoBarco;

                arrayContBarcos[tamanoBarco]--;
                System.out.println(arrayContBarcos[tamanoBarco]);

                if (arrayContBarcos[tamanoBarco] <= 0) {
                    return false;
                }

                //  numBarcosJugador[JUGADOR]++;
                //     this.mostrar();
//                if (contBarco1 >= 0 && tamanoBarco == 1) {
//                    contBarco1--;
//                    System.out.println(contBarco1);
//                    if (contBarco2 <= 0 && tamanoBarco == 2) {
//                        contBarco2--;
//                        System.out.println(contBarco2);
//                        if (contBarco3 >= 0 && tamanoBarco == 3) {
//                            contBarco3--;
//                            System.out.println(contBarco3);
//                            if (contBarco4 >= 0 && tamanoBarco == 4) {
//                                contBarco4--;
//                                System.out.println(contBarco4);
//                            }
//                        }
//                    }
//                }
            }

            for (int a = 0; a < tamanoBarco; a++) {
                if (orientacion == HORIZONTAL) {
                    arrayTablero[x + a][y] = (byte) tamanoBarco;
                } else {
                    arrayTablero[x][y + a] = (byte) tamanoBarco;
                }

            }

            return true;
            // Controlar que las posiciones indicadas del array son correctas
        } catch (ArrayIndexOutOfBoundsException ex) {
            ex.printStackTrace();
            return false;
        }

    }

    @Override
    public String toString() {
        String strTablero = "";
        // TODO: Hacer esto con bucles
        for (int y = 0; y < arrayTablero.length; y++) {
            strTablero += "\n";
            for (int x = 0; x < arrayTablero.length; x++) {
                strTablero += arrayTablero[x][y];

            }
        }
        return strTablero;
    }

    public byte getNumBarcos(byte jugador) {
        return numBarcosJugador[jugador];
    }

}
