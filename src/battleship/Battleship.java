/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Victor
 */
public class Battleship extends Application {

    @Override
    public void start(Stage primaryStage) {
        Tablero tablero = new Tablero();
        Disparos dispararBarco = new Disparos();
        dispararBarco.setTablero(tablero);
        //tablero.inicializar();

        //crear ventana de formulario.
        //elementos principales de la ventana.
        GridPane grid = new GridPane();
        grid.setAlignment(Pos.CENTER);
        grid.setHgap(10);
        grid.setVgap(10);
        grid.setPadding(new Insets(25, 25, 25, 25));
        Scene scene = new Scene(grid, 750, 500);
        primaryStage.setTitle("BattleShip");
        primaryStage.setScene(scene);
        primaryStage.show();

        //Campos del texto del formulario.
        Label labelFila = new Label("Fila:");
        grid.add(labelFila, 0, 0);
        TextField textFieldFila = new TextField();
        grid.add(textFieldFila, 1, 0);
        textFieldFila.setPromptText("Del 0 al 9");

        Label labelColumna = new Label("Columna");
        grid.add(labelColumna, 0, 1);
        TextField textFieldColumna = new TextField();
        grid.add(textFieldColumna, 1, 1);
        textFieldColumna.setPromptText("Del 0 al 9");

        Label labelTamano = new Label("Tamaño del Barco");
        grid.add(labelTamano, 0, 2);
        TextField textFieldTamano = new TextField();
        grid.add(textFieldTamano, 1, 2);
        textFieldTamano.setPromptText("Maximo 4");

        Label labelOrientacion = new Label("Orientacion");
        grid.add(labelOrientacion, 0, 3);
        TextField textFieldOrientacion = new TextField();
        grid.add(textFieldOrientacion, 1, 3);
        textFieldOrientacion.setPromptText("0 o 1");

        // Tablero en pantalla.
        Label labelTablero = new Label("");
        labelTablero.setText(tablero.toString());
        labelTablero.setMinHeight(50);
        grid.add(labelTablero, 0, 4);

        //ERRORES.
        Label labelError = new Label("");
        grid.add(labelError, 0, 6);

        //Tablero para disparos.
        Label labelDisparo = new Label("");
        labelDisparo.setText(dispararBarco.toString());
        labelDisparo.setMinHeight(50);
        grid.add(labelDisparo, 2, 4);

        //Barcos Tamaño 4.
        Label labelBarco4 = new Label("Barcos Tamaño 4:");
        grid.add(labelBarco4, 2, 0);

        //Barcos Tamaño 3.
        Label labelBarco3 = new Label("Barcos Tamaño 3:");
        grid.add(labelBarco3, 2, 1);

        //Barcos Tamaño 2.
        Label labelBarco2 = new Label("Barcos Tamaño 2:");
        grid.add(labelBarco2, 2, 2);

        //Barcos Tamaño 1.
        Label labelBarco1 = new Label("Barcos Tamaño 1:");
        grid.add(labelBarco1, 2, 3);

        //Boton Disparo
        Button btnDisparo = new Button("Disparar a Barco");
        grid.add(btnDisparo, 2, 5);
        btnDisparo.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                int x = Integer.valueOf(textFieldColumna.getText());
                int y = Integer.valueOf(textFieldFila.getText());
                dispararBarco.disparoBarco(x, y);
                labelDisparo.setText(dispararBarco.toString());

            }

        });

        // Botón "Colocar Barco"
        Button btnColocarFicha = new Button("Colocar Barco");
        grid.add(btnColocarFicha, 0, 5);
        btnColocarFicha.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent e) {
                try {
                    // Obtener datos introducidos por el usuario
                    int x = Integer.valueOf(textFieldColumna.getText());
                    int y = Integer.valueOf(textFieldFila.getText());
                    int orientacion = Integer.valueOf(textFieldOrientacion.getText());
                    int tamanoBarco = Integer.valueOf(textFieldTamano.getText());
                    // Colocar la ficha indicada por el usuario
                    tablero.ponerBarco(x, y, tamanoBarco, orientacion);
                    // Mostrar el tablero como texto
                    labelTablero.setText(tablero.toString());

                    // Controlar que los valores indicados son numéricos
                } catch (NumberFormatException ex) {
                    labelError.setText("Valor incorrecto");
                    ex.printStackTrace();
                }
            }
        });
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
