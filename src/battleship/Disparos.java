/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package battleship;

/**
 *
 * @author Victor
 */
public class Disparos {

    Tablero Tablero;

    private static final char AGUA = 'A';
    private static final char TOCADO = 'T';

    private byte[][] arrayDisparos = new byte[10][10];

    public Disparos() {
        for (int x = 0; x < arrayDisparos.length; x++) {
            for (int y = 0; y < arrayDisparos.length; y++) {
                arrayDisparos[x][y] = (char) '0';
            }
        }
    }

    void setTablero(Tablero tablero) {
        this.Tablero = tablero;
    }

    public char disparoBarco(int x, int y) {
        if (Tablero.arrayTablero[x][y] == 0) {
            arrayDisparos[x][y] = (char) AGUA;
            return AGUA;

        } else {
            arrayDisparos[x][y] = (char) TOCADO;
            return TOCADO;
        }
    }

    @Override
    public String toString() {
        String strDisparo = "";
        // TODO: Hacer esto con bucles
        for (int y = 0; y < arrayDisparos.length; y++) {
            strDisparo += "\n";
            for (int x = 0; x < arrayDisparos.length; x++) {
                strDisparo += (char) arrayDisparos[x][y];

            }
        }
        return strDisparo;
    }
}
